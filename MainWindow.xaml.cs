﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Net;
using System.IO;
using Path = System.IO.Path;
using System.Windows.Forms;
using MessageBox = System.Windows.MessageBox;
using Excel = Microsoft.Office.Interop.Excel;
using System.ComponentModel;

namespace laba2
{
    public partial class MainWindow : System.Windows.Window
    {
        int currentpage = 0;
        List<Note> notes = new List<Note>();
        List<ShortNote> shortnotes = new List<ShortNote>();
        List<Note> refreshnotes = new List<Note>();      
        string remoteUri = "https://bdu.fstec.ru/documents/files/";
        string fileName = "thrlist.xlsx", myStringWebResource;
        WebClient myWebClient = new WebClient();
        public MainWindow()
        {
            InitializeComponent();
        }
        public Excel.Application excelApp = new Excel.Application
        {
            Visible = false,
        };
        private void frm_main_Loaded(object sender, RoutedEventArgs e)
        {
            btnOK.Visibility = Visibility.Hidden;
            btn_prev.Visibility = Visibility.Hidden;
            dgv.EndEdit();
            if (File.Exists(Path.GetFullPath(fileName)))
            {
                excelApp.Workbooks.Open(Path.GetFullPath(fileName));
                Excel.Worksheet currentSheet = (Excel.Worksheet)excelApp.Workbooks[1].Worksheets[1];
                int lastrow = currentSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;
                for (int i = 3; i <= lastrow; i++)
                {
                    notes.Add(new Note
                    {
                        ThreatID = (int)currentSheet.Cells[i, 1].Value,
                        ThreatName = currentSheet.Cells[i, 2].Value.ToString(),
                        ThreatDescription = currentSheet.Cells[i, 3].Value.ToString(),
                        ThreatSource = currentSheet.Cells[i, 4].Value.ToString(),
                        ThreatObject = currentSheet.Cells[i, 5].Value.ToString(),
                        ConfidentialityViolation = Convert.ToBoolean(currentSheet.Cells[i, 6].Value),
                        IntegrityViolation = Convert.ToBoolean(currentSheet.Cells[i, 7].Value),
                        AccessibilityViolation = Convert.ToBoolean(currentSheet.Cells[i, 8].Value),
                    });
                };
                View20(currentpage);
                dgvColumns();
                dgv.AutoResizeColumns();
                excelApp.Quit();
            }
            else
            {
                MessageBox.Show("Файл не существует, его необходимо загрузить!", "Файл отсутствует", MessageBoxButton.OK, MessageBoxImage.Error);

                try
                {
                    myStringWebResource = remoteUri + fileName;
                    MessageBox.Show($"Downloading File \"{fileName}\" from \"{myStringWebResource}\" .......\n\n");
                    myWebClient.DownloadFile(myStringWebResource, fileName);
                    myWebClient = new WebClient();
                    MessageBox.Show($"Файл {fileName} из {myStringWebResource} успешно загружен в папку {Path.GetFullPath(fileName)} !");
                    btn_next.Visibility = System.Windows.Visibility.Hidden;
                    btn_prev.Visibility = System.Windows.Visibility.Hidden;
                    btn_refresh.Visibility = System.Windows.Visibility.Visible;
                    btn_show.Visibility = System.Windows.Visibility.Visible;
                    btn_short.Visibility = System.Windows.Visibility.Visible;
                    btn_safe.Visibility = System.Windows.Visibility.Visible;
                }
                catch (Exception)
                {
                    MessageBox.Show("Что-то пошло не так(( Попробуйте ещё раз!");
                }
            }
        }
        private void btn_show_Click(object sender, RoutedEventArgs e)
        {
            WFH.Visibility = Visibility.Visible;
            dgv.Show();
            if (notes.Count == 0)
            {
                btn_next.Visibility = System.Windows.Visibility.Visible;
                excelApp.Workbooks.Open(Path.GetFullPath(fileName));
                Excel.Worksheet currentSheet = (Excel.Worksheet)excelApp.Workbooks[1].Worksheets[1];
                int lastrow = currentSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;
                for (int i = 3; i <= lastrow; i++)
                {
                    notes.Add(new Note
                    {
                        ThreatID = (int)currentSheet.Cells[i, 1].Value,
                        ThreatName = currentSheet.Cells[i, 2].Value.ToString(),
                        ThreatDescription = currentSheet.Cells[i, 3].Value.ToString(),
                        ThreatSource = currentSheet.Cells[i, 4].Value.ToString(),
                        ThreatObject = currentSheet.Cells[i, 5].Value.ToString(),
                        ConfidentialityViolation = Convert.ToBoolean(currentSheet.Cells[i, 6].Value),
                        IntegrityViolation = Convert.ToBoolean(currentSheet.Cells[i, 7].Value),
                        AccessibilityViolation = Convert.ToBoolean(currentSheet.Cells[i, 8].Value),
                    });
                };
            }
            currentpage = 0;
            btn_next.Visibility = System.Windows.Visibility.Visible;
            btn_prev.Visibility = System.Windows.Visibility.Hidden;
            View20(currentpage);
            dgvColumns();
            dgv.AutoResizeColumns();
            excelApp.Quit();
        }
        private void btn_short_Click(object sender, RoutedEventArgs e)
        {
            WFH.Visibility = Visibility.Visible;
            dgv.Show();
            btn_prev.Visibility = System.Windows.Visibility.Hidden;
            btn_next.Visibility = System.Windows.Visibility.Hidden;
            excelApp.Workbooks.Open(Path.GetFullPath(fileName));
            Excel.Worksheet currentSheet = (Excel.Worksheet)excelApp.Workbooks[1].Worksheets[1];

            for (int i = 3; i <= 218; i++)
            {
                shortnotes.Add(new ShortNote
                {
                    ThreatID = "УБИ." + currentSheet.Cells[i, 1].Value,
                    ThreatName = currentSheet.Cells[i, 2].Value.ToString(),
                });
            };
            var bindingList = new BindingList<ShortNote>(shortnotes);
            var source = new BindingSource(bindingList, null);
            dgv.DataSource = null;
            dgv.DataSource = source;
            dgv.Columns[0].HeaderText = "Идентификатор УБИ";
            dgv.Columns[1].HeaderText = "Наименование УБИ";
            dgv.AutoResizeColumns();
            excelApp.Quit();
        }
        private void btn_refresh_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                myStringWebResource = remoteUri + fileName;
                MessageBox.Show($"Downloading File \"{fileName}\" from \"{myStringWebResource}\" .......\n\n");
                myWebClient.DownloadFile(myStringWebResource, fileName);
                myWebClient = new WebClient();
                MessageBox.Show($"Файл {fileName} из {myStringWebResource} успешно загружен в папку {Path.GetFullPath(fileName)} !");
                excelApp.Workbooks.Open(Path.GetFullPath(fileName));
                excelApp.Visible = false;
                Excel.Worksheet currentSheet = (Excel.Worksheet)excelApp.Workbooks[1].Worksheets[1];

                for (int i = 3; i <= 218; i++)
                {
                    refreshnotes.Add(new Note
                    {
                        ThreatID = (int)currentSheet.Cells[i, 1].Value,
                        ThreatName = currentSheet.Cells[i, 2].Value.ToString(),
                        ThreatDescription = currentSheet.Cells[i, 3].Value.ToString(),
                        ThreatSource = currentSheet.Cells[i, 4].Value.ToString(),
                        ThreatObject = currentSheet.Cells[i, 5].Value.ToString(),
                        ConfidentialityViolation = Convert.ToBoolean(currentSheet.Cells[i, 6].Value),
                        IntegrityViolation = Convert.ToBoolean(currentSheet.Cells[i, 7].Value),
                        AccessibilityViolation = Convert.ToBoolean(currentSheet.Cells[i, 8].Value),
                    });
                };
                excelApp.Quit();
                int countrefreshnotes = 0;
                WFH.Visibility = Visibility.Hidden;
                txtRef.Visibility = Visibility.Visible;
                for (int i = 0; i < notes.Count; i++)
                {
                    if (!Compare(notes[i], refreshnotes[i], txtRef))
                    {
                        notes.RemoveAt(i);
                        notes.Insert(i, refreshnotes[i]);

                        countrefreshnotes++;
                    }
                }
                MessageBox.Show($"Обновление прошло успешно! Количество обновленных записей - {countrefreshnotes}");
                btn_next.Visibility = System.Windows.Visibility.Hidden;
                btn_prev.Visibility = System.Windows.Visibility.Hidden;
                btn_refresh.Visibility = System.Windows.Visibility.Hidden;
                btn_show.Visibility = System.Windows.Visibility.Hidden;
                btn_short.Visibility = System.Windows.Visibility.Hidden;
                btn_safe.Visibility = System.Windows.Visibility.Hidden;
                dgv.Hide();
                txtRef.Visibility = System.Windows.Visibility.Visible;
                btnOK.Visibility = System.Windows.Visibility.Visible;
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка обновления данных!");
            }
        }
        private void btn_next_Click(object sender, RoutedEventArgs e)
        {
            excelApp.Workbooks.Open(Path.GetFullPath(fileName));
            Excel.Worksheet currentSheet = (Excel.Worksheet)excelApp.Workbooks[1].Worksheets[1];
            int lastrow = currentSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;
            if (currentpage == notes.Count() / 20 - 1)
            {
                currentpage++;
                BindingList<Note> bindingList = new BindingList<Note>(notes);
                BindingSource source = new BindingSource();
                for (int i = 0; i < notes.Count % 20; i++)
                {
                    source.Add(bindingList[i + currentpage * 20]);
                }
                dgv.DataSource = source;
                btn_next.Visibility = System.Windows.Visibility.Hidden;
                btn_prev.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                currentpage++;
                View20(currentpage);
                btn_prev.Visibility = System.Windows.Visibility.Visible;
            }
        }
        public void View20(int pagenumber)
        {
            BindingList<Note> bindingList = new BindingList<Note>(notes);
            BindingSource source = new BindingSource();
            for (int i = 0; i < 20; i++)
            {
                source.Add(bindingList[i + pagenumber * 20]);
            }
            dgv.DataSource = source;
        }
        private void btn_prev_Click(object sender, RoutedEventArgs e)
        {
            excelApp.Workbooks.Open(Path.GetFullPath(fileName));
            Excel.Worksheet currentSheet = (Excel.Worksheet)excelApp.Workbooks[1].Worksheets[1];
            int lastrow = currentSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;
            if (currentpage < 2)
            {
                currentpage--;
                View20(currentpage);
                btn_prev.Visibility = System.Windows.Visibility.Hidden;
                btn_next.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                currentpage--;
                View20(currentpage);
                btn_prev.Visibility = System.Windows.Visibility.Visible;
                btn_next.Visibility = System.Windows.Visibility.Visible;
            }
        }
        private void btn_safe_Click(object sender, RoutedEventArgs e)
        {
            Excel.Application newExcelApp = new Excel.Application();
            newExcelApp.Visible = false;
            newExcelApp.SheetsInNewWorkbook = 3;
            newExcelApp.Workbooks.Add();
            Excel.Worksheet currentSheet2 = (Excel.Worksheet)newExcelApp.Workbooks[1].Worksheets[1];
            currentSheet2.Name = "УБИ";
            currentSheet2.Cells[1, 1].Value = "Идентификатор УБИ";
            currentSheet2.Cells[1, 2].Value = "Наименование УБИ";
            currentSheet2.Cells[1, 3].Value = "Описание";
            currentSheet2.Cells[1, 4].Value = "Источник угрозы (характеристика и потенциал нарушителя)";
            currentSheet2.Cells[1, 5].Value = "Объект воздействия";
            currentSheet2.Cells[1, 6].Value = "Нарушение конфиденциальности";
            currentSheet2.Cells[1, 7].Value = "Нарушение целостности";
            currentSheet2.Cells[1, 8].Value = "Нарушение доступности";

            for (int i = 0; i < notes.Count; i++)
            {
                currentSheet2.Cells[i + 2, 1].Value = notes[i].ThreatID;
                currentSheet2.Cells[i + 2, 2].Value = notes[i].ThreatName;
                currentSheet2.Cells[i + 2, 3].Value = notes[i].ThreatDescription;
                currentSheet2.Cells[i + 2, 4].Value = notes[i].ThreatSource;
                currentSheet2.Cells[i + 2, 5].Value = notes[i].ThreatObject;
                currentSheet2.Cells[i + 2, 6].Value = notes[i].ConfidentialityViolation;
                currentSheet2.Cells[i + 2, 7].Value = notes[i].IntegrityViolation;
                currentSheet2.Cells[i + 2, 8].Value = notes[i].AccessibilityViolation;
            };
            newExcelApp.AlertBeforeOverwriting = false;
            SaveFileDialog sfd = new SaveFileDialog()
            {
                Filter = "MS Excel dosuments (*.xlsx)|*.xlsx",
                DefaultExt = "*.xlsx",
                FileName = "1",
                Title = "Укажите директорию и имя файла для сохранения"
            };
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                currentSheet2.SaveAs(sfd.FileName);
                MessageBox.Show("Файл успешно сохранен!");
            }
            else
            {
                MessageBox.Show("Что-то пошло не так!");
            }
            newExcelApp.Quit();
        }
        private void frm_main_Closing(object sender, CancelEventArgs e)
        {
            excelApp.Quit();
        }
        private static bool Compare(Note note, Note newNote, System.Windows.Controls.TextBox txtRef)
        {
            bool equal = true;
            if (note.ThreatID != newNote.ThreatID)
            {
                equal = false;
                txtRef.Text += note.ThreatID + "\r\n" + note.ThreatID + " => " + newNote.ThreatID + "\r\n";
                txtRef.Text += "_______________________________________________________________________________________ \r\n";
            }
            if (note.ThreatDescription != newNote.ThreatDescription)
            {
                equal = false;
                txtRef.Text += note.ThreatID + "\r\n" + note.ThreatDescription + " => " + newNote.ThreatDescription + "\r\n";
                txtRef.Text += "_______________________________________________________________________________________ \r\n";
            }
            if (note.ThreatName != newNote.ThreatName)
            {
                equal = false;
                txtRef.Text += note.ThreatID + "\r\n" + note.ThreatName + " => " + newNote.ThreatName + "\r\n";
                txtRef.Text += "_______________________________________________________________________________________ \r\n";
            }
            if (note.ThreatObject != newNote.ThreatObject)
            {
                equal = false;
                txtRef.Text += note.ThreatID + "\r\n" + note.ThreatObject + " => " + newNote.ThreatObject + "\r\n";
                txtRef.Text += "_______________________________________________________________________________________ \r\n";
            }
            if (note.ThreatSource != newNote.ThreatSource)
            {
                equal = false;
                txtRef.Text += note.ThreatID + "\r\n" + note.ThreatSource + " => " + newNote.ThreatSource + "\r\n";
                txtRef.Text += "_______________________________________________________________________________________ \r\n";
            }
            if (note.ConfidentialityViolation != newNote.ConfidentialityViolation)
            {
                equal = false;
                txtRef.Text += note.ThreatID + "\r\n" + note.ConfidentialityViolation + " => " + newNote.ConfidentialityViolation + "\r\n";
                txtRef.Text += "_______________________________________________________________________________________ \r\n";
            }
            if (note.IntegrityViolation != newNote.IntegrityViolation)
            {
                equal = false;
                txtRef.Text += note.ThreatID + "\r\n" + note.IntegrityViolation + " => " + newNote.IntegrityViolation + "\r\n";
                txtRef.Text += "_______________________________________________________________________________________ \r\n";
            }
            if (note.AccessibilityViolation != newNote.AccessibilityViolation)
            {
                equal = false;
                txtRef.Text += note.ThreatID + "\r\n" + note.AccessibilityViolation + " => " + newNote.AccessibilityViolation + "\r\n";
                txtRef.Text += "_______________________________________________________________________________________ \r\n";
            }
            return equal;
        }
        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            txtRef.Visibility = Visibility.Hidden;
            btnOK.Visibility = Visibility.Hidden;
            dgv.Show();
            btn_next.Visibility = Visibility.Hidden;
            btn_prev.Visibility = Visibility.Hidden;
            btn_refresh.Visibility = Visibility.Visible;
            btn_show.Visibility = Visibility.Visible;
            btn_short.Visibility = Visibility.Visible;
            btn_safe.Visibility = Visibility.Visible;
            btn_show_Click(sender, e);
        }
        public void dgvColumns()
        {
            dgv.Columns[0].HeaderText = "Идентификатор УБИ";
            dgv.Columns[1].HeaderText = "Наименование УБИ";
            dgv.Columns[2].HeaderText = "Описание";
            dgv.Columns[3].HeaderText = "Источник угрозы (характеристика и потенциал нарушителя)";
            dgv.Columns[4].HeaderText = "Объект воздействия";
            dgv.Columns[5].HeaderText = "Нарушение конфиденциальности";
            dgv.Columns[6].HeaderText = "Нарушение целостности";
            dgv.Columns[7].HeaderText = "Нарушение доступности";
        }
    }
}