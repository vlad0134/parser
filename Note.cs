﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba2
{
    class Note
    {
        public int ThreatID { get; set; }
        public string ThreatName { get; set; }
        public string ThreatDescription { get; set; }
        public string ThreatSource { get; set; }
        public string ThreatObject { get; set; }
        public bool ConfidentialityViolation { get; set; }
        public bool IntegrityViolation { get; set; }
        public bool AccessibilityViolation { get; set; }
    }
}
